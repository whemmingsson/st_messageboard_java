package com.hemmingsson.msgboard;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hemmingsson.msgboard.abstractions.MessageRepository;
import com.hemmingsson.msgboard.abstractions.UserRepository;
import com.hemmingsson.msgboard.controllers.MessageController;
import com.hemmingsson.msgboard.models.Message;
import com.hemmingsson.msgboard.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/*
 Note that these unit tests DO NOT test the request filter that deals with authentication. For that, an integration test is more suitable
 */
@WebMvcTest(MessageController.class)
@ContextConfiguration(classes=MessageboardApplication.class)
class MessageControllerTests {

	private MockMvc mvc;

	@MockBean
	private UserRepository userRepo;

	@MockBean
	private MessageRepository messageRepo;

	@Autowired
	private WebApplicationContext webApplicationContext;

	private Message getFirstMessage(){
		Message m1 = new Message();
		m1.setId(0);
		m1.setText("Mockito is awesome");
		m1.setUserId(0);
		return m1;
	}

	private Message getSecondMessage(){
		Message m2 = new Message();
		m2.setId(1);
		m2.setText("Thanks to Mockbeans, we can mock internal dependencies");
		m2.setUserId(0);
		return m2;
	}

	private Message getThirdMessage(){
		Message m3 = new Message();
		m3.setId(2);
		m3.setText("This message was posted by user 99");
		m3.setUserId(99);
		return m3;
	}

	@BeforeEach
	public void setUp() {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void getAllMessages_ReturnOneResult_OK()
			throws Exception {

		Message m1 = getFirstMessage();

		List<Message> allMessages = Arrays.asList(m1);

		given(messageRepo.findAll()).willReturn(allMessages);

		mvc.perform(get("/v1/messages/all")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)));
	}

	@Test
	public void getAllMessages_ReturnZeroResults_OK()
			throws Exception {

		mvc.perform(get("/v1/messages/all")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(0)));
	}

	@Test
	public void getAllMessages_ReturnTwoResults_OK()
			throws Exception {

		Message m1 = getFirstMessage();
		Message m2 = getSecondMessage();

		List<Message> allMessages = Arrays.asList(m1, m2);

		given(messageRepo.findAll()).willReturn(allMessages);

		mvc.perform(get("/v1/messages/all")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)));
	}

	@Test
	public void getAllMessages_ReturnOneResultVerifyData_OK()
			throws Exception {

		Message m1 = getFirstMessage();

		List<Message> allMessages = Arrays.asList(m1);

		given(messageRepo.findAll()).willReturn(allMessages);

		mvc.perform(get("/v1/messages/all")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].text", is(m1.getText())))
				.andExpect(jsonPath("$[0].id", is(m1.getId())))
				.andExpect(jsonPath("$[0].userId", is(m1.getUserId())));
	}

	@Test
	public void getAllMessages_ThrowsUnexpectedException_INTERNALERROR()
			throws Exception {

		given(messageRepo.findAll()).willThrow(NullPointerException.class);

		mvc.perform(get("/v1/messages/all")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isInternalServerError())
				.andExpect(jsonPath("$.type", is("/errors/internal-server-error")))
				.andExpect(jsonPath("$.status", is(500)));

	}

	@Test
	public void getMessage_MessageExists_OK()
			throws Exception {

		Message m1 = getFirstMessage();

		given(messageRepo.findById(m1.getId())).willReturn(java.util.Optional.of(m1));

		mvc.perform(get("/v1/messages/id/" + m1.getId())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.text", is(m1.getText())))
				.andExpect(jsonPath("$.id", is(m1.getId())))
				.andExpect(jsonPath("$.userId", is(m1.getUserId())));
	}

	@Test
	public void getMessage_MessageDoesNotExist_NOTFOUND()
			throws Exception {

		Message m1 = getFirstMessage();

		given(messageRepo.findById(m1.getId())).willReturn(java.util.Optional.empty());

		mvc.perform(get("/v1/messages/id/" + m1.getId())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());

	}

	@Test
	public void getMessage_MessageDoesNotExist_CheckAPIError_NOTFOUND()
			throws Exception {

		Message m1 = getFirstMessage();

		given(messageRepo.findById(m1.getId())).willReturn(java.util.Optional.empty());

		mvc.perform(get("/v1/messages/id/" + m1.getId())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.type", is("/errors/validation/message-id-not-found")))
				.andExpect(jsonPath("$.status", is(404)));
	}

	@Test
	public void getMessage_ThrowsUnexpectedException_INTERNALERROR()
			throws Exception {

		Message m1 = getFirstMessage();

		given(messageRepo.findById(m1.getId())).willThrow(NullPointerException.class);

		mvc.perform(get("/v1/messages/id/" + m1.getId())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isInternalServerError())
				.andExpect(jsonPath("$.type", is("/errors/internal-server-error")))
				.andExpect(jsonPath("$.status", is(500)));
	}

	@Test
	public void getMessagesByUserId_OneMessageExists_OK()
			throws Exception {

		Message m1 = getFirstMessage();
		Message m3 = getThirdMessage();

		given(messageRepo.findAll()).willReturn(Arrays.asList(m1,m3));

		mvc.perform(get("/v1/messages/userid/" + m1.getUserId())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].text", is(m1.getText())))
				.andExpect(jsonPath("$[0].id", is(m1.getId())))
				.andExpect(jsonPath("$[0].userId", is(m1.getUserId())));
	}

	@Test
	public void getMessagesByUserId_TwoMessagesExists_OK()
			throws Exception {

		Message m1 = getFirstMessage();
		Message m2 = getSecondMessage();
		Message m3 = getThirdMessage();

		given(messageRepo.findAll()).willReturn(Arrays.asList(m1,m2,m3));

		mvc.perform(get("/v1/messages/userid/" + m1.getUserId())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].text", is(m1.getText())))
				.andExpect(jsonPath("$[0].id", is(m1.getId())))
				.andExpect(jsonPath("$[0].userId", is(m1.getUserId())))
				.andExpect(jsonPath("$[1].text", is(m2.getText())))
				.andExpect(jsonPath("$[1].id", is(m2.getId())))
				.andExpect(jsonPath("$[1].userId", is(m2.getUserId())));
	}

	@Test
	public void getMessagesByUserId_MessageDoesNotExist_NOTFOUND()
			throws Exception {

		Message m1 = getFirstMessage();

		given(messageRepo.findAll()).willReturn(Arrays.asList());

		mvc.perform(get("/v1/messages/userid/" + m1.getUserId())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void getMessagesByUserId_MessageDoesNotExist_CheckAPIError_NOTFOUND()
			throws Exception {

		Message m1 = getFirstMessage();

		given(messageRepo.findAll()).willReturn(Arrays.asList());

		mvc.perform(get("/v1/messages/userid/" + m1.getUserId())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.type", is("/errors/validation/messages-not-found")))
				.andExpect(jsonPath("$.status", is(404)));
	}

	@Test
	public void postNewMessage_CREATED()
			throws Exception {

		Message m1 = getFirstMessage();

		User u1 = new User(0,"mocky");
		u1.setActivetoken("mocktoken");

		given(userRepo.existsById(m1.getUserId())).willReturn(true);
		given(userRepo.findByActivetoken(u1.getActivetoken())).willReturn(u1);

		ObjectMapper mapper = new ObjectMapper();
		String jsonBodyContent = mapper.writeValueAsString(m1);

		mvc.perform(post("/v1/messages/")
				.content(jsonBodyContent)
				.header("Authorization", u1.getActivetoken())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.text", is(m1.getText())))
				.andExpect(jsonPath("$.id", is(m1.getId())))
				.andExpect(jsonPath("$.userId", is(m1.getUserId())))
				.andExpect(header().exists("location"));
	}

	@Test
	public void postNewMessage_VerifyHtmlSanitizer_CREATED()
			throws Exception {

		Message m1 = getFirstMessage();
		m1.setText("script:<script>alert('XSS');</script>");

		User u1 = new User(0,"mocky");
		u1.setActivetoken("mocktoken");

		given(userRepo.existsById(m1.getUserId())).willReturn(true);
		given(userRepo.findByActivetoken(u1.getActivetoken())).willReturn(u1);

		ObjectMapper mapper = new ObjectMapper();
		String jsonBodyContent = mapper.writeValueAsString(m1);

		mvc.perform(post("/v1/messages/")
				.content(jsonBodyContent)
				.header("Authorization", u1.getActivetoken())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.text", is("script:")));
	}

	@Test
	public void postNewMessage_MissingText_BADREQUEST()
			throws Exception {

		Message m1 = getFirstMessage();
		m1.setText(null);

		given(userRepo.existsById(m1.getUserId())).willReturn(true);

		ObjectMapper mapper = new ObjectMapper();
		String jsonBodyContent = mapper.writeValueAsString(m1);

		 mvc.perform(post("/v1/messages/")
				.content(jsonBodyContent)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void postNewMessage_NoUserWithId_BADREQUEST()
			throws Exception {

		Message m1 = getFirstMessage();

		given(userRepo.existsById(m1.getUserId())).willReturn(false);

		ObjectMapper mapper = new ObjectMapper();
		String jsonBodyContent = mapper.writeValueAsString(m1);

		mvc.perform(post("/v1/messages/")
				.content(jsonBodyContent)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void postNewMessage_NoUserWithId_and_MissingText_BADREQUEST()
			throws Exception {

		Message m1 = getFirstMessage();
		m1.setText(null);

		User u1 = new User(0,"mocky");
		u1.setActivetoken("mocktoken");

		given(userRepo.existsById(m1.getUserId())).willReturn(false);
		given(userRepo.findByActivetoken(u1.getActivetoken())).willReturn(u1);

		ObjectMapper mapper = new ObjectMapper();
		String jsonBodyContent = mapper.writeValueAsString(m1);

		mvc.perform(post("/v1/messages/")
				.content(jsonBodyContent)
				.header("Authorization", u1.getActivetoken())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errors", hasSize(2)));

	}

	@Test
	public void postNewMessage_TextToLong_BADREQUEST()
			throws Exception {

		Message m1 = getFirstMessage();

		User u1 = new User(0,"mocky");
		u1.setActivetoken("mocktoken");

		// The message contains 600 characters which is well above limit of 512
		m1.setText("l31CviTL2DriFKJaf0AUgivCHabc7C6Tl5eMKNE6GgaYOPLWsChl7piuH554sBm8c6lUCIWca8ThGngin7PfVBQb37Ah3qQs1lguObJ2qOx2XYKle5ZktA6VhKfezUsxLCJp9QVKPyoNJu4MScfGMfV9cWAckraaxOt2Wu7mxw0rs400fKJidZOxDYjItzjujM4IfLtqJJbr7ef0YaMva62kR9T3VPBF6qOelY73PaycGh1LeFz4vqSdceW9x1H9P3MAMpmtmMaxIUV0YhUxIzNEPaDl0UiP87KAl0U6Dvkzl31CviTL2DriFKJaf0AUgivCHabc7C6Tl5eMKNE6GgaYOPLWsChl7piuH554sBm8c6lUCIWca8ThGngin7PfVBQb37Ah3qQs1lguObJ2qOx2XYKle5ZktA6VhKfezUsxLCJp9QVKPyoNJu4MScfGMfV9cWAckraaxOt2Wu7mxw0rs400fKJidZOxDYjItzjujM4IfLtqJJbr7ef0YaMva62kR9T3VPBF6qOelY73PaycGh1LeFz4vqSdceW9x1H9P3MAMpmtmMaxIUV0YhUxIzNEPaDl0UiP87KAl0U6Dvkz");

		given(userRepo.existsById(m1.getUserId())).willReturn(true);

		ObjectMapper mapper = new ObjectMapper();
		String jsonBodyContent = mapper.writeValueAsString(m1);

		mvc.perform(post("/v1/messages/")
				.content(jsonBodyContent)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void postNewMessage_UnexpectedError_INTERNALSERVERERROR()
			throws Exception {

		Message m1 = getFirstMessage();

		User u1 = new User(0,"mocky");
		u1.setActivetoken("mocktoken");

		given(userRepo.existsById(m1.getUserId())).willThrow(NullPointerException.class);
		given(userRepo.findByActivetoken(u1.getActivetoken())).willReturn(u1);

		ObjectMapper mapper = new ObjectMapper();
		String jsonBodyContent = mapper.writeValueAsString(m1);

		mvc.perform(post("/v1/messages/")
				.content(jsonBodyContent)
				.header("Authorization", u1.getActivetoken())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isInternalServerError());
	}

	@Test
	public void updateMessage_OK()
			throws Exception {

		Message m1 = getFirstMessage();
		User u1 = new User(0,"mocky");
		u1.setActivetoken("mocktoken");

		Message m1_updated = new Message();
		m1_updated.setText("This is the new text");
		m1_updated.setId(m1.getId());
		m1_updated.setUserId(m1.getUserId());

		given(userRepo.existsById(m1.getUserId())).willReturn(true);
		given(messageRepo.findAll()).willReturn(Arrays.asList(m1));
		given(userRepo.findByActivetoken(u1.getActivetoken())).willReturn(u1);

		ObjectMapper mapper = new ObjectMapper();
		String jsonBodyContent = mapper.writeValueAsString(m1_updated);

		mvc.perform(put("/v1/messages/")
				.content(jsonBodyContent)
				.header("Authorization", u1.getActivetoken())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.text", is(m1_updated.getText())))
				.andExpect(jsonPath("$.id", is(m1_updated.getId())))
				.andExpect(jsonPath("$.userId", is(m1_updated.getUserId())))
				.andExpect(header().exists("location"));
	}

	@Test
	public void updateMessage_NoMessageWithId_NOTFOUND()
			throws Exception {

		Message m1 = getFirstMessage();
		User u1 = new User(0,"mocky");
		u1.setActivetoken("mocktoken");

		given(userRepo.existsById(m1.getUserId())).willReturn(true);
		given(messageRepo.findAll()).willReturn(Arrays.asList(m1));
		given(userRepo.findByActivetoken(u1.getActivetoken())).willReturn(u1);

		Message m1_updated = new Message();
		m1_updated.setText("This is the new text");
		m1_updated.setId(77);
		m1_updated.setUserId(m1.getUserId());

		ObjectMapper mapper = new ObjectMapper();
		String jsonBodyContent = mapper.writeValueAsString(m1_updated);

		mvc.perform(put("/v1/messages/")
				.content(jsonBodyContent)
				.header("Authorization", u1.getActivetoken())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void updateMessage_UserMismatch_WithWrongUserId_UNAUTHORIZED()
			throws Exception {

		Message m1 = getFirstMessage();

		Message m1_updated = new Message();
		m1_updated.setText("This is the new text");
		m1_updated.setId(m1.getId());
		m1_updated.setUserId(77);

		User u1 = new User(0,"mocky");
		u1.setActivetoken("mocktoken");

		given(userRepo.existsById(m1_updated.getUserId())).willReturn(true);
		given(messageRepo.findAll()).willReturn(Arrays.asList(m1));
		given(userRepo.findByActivetoken(u1.getActivetoken())).willReturn(u1);

		ObjectMapper mapper = new ObjectMapper();
		String jsonBodyContent = mapper.writeValueAsString(m1_updated);

		mvc.perform(put("/v1/messages/")
				.content(jsonBodyContent)
				.header("Authorization", u1.getActivetoken()) // Token here, "mocktoken", is the token for user with id 0 who created the original message. The userid sent in the request was 77.
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnauthorized());
	}

	@Test
	public void updateMessage_UserNowFound_BADREQUEST()
			throws Exception {

		Message m1 = getFirstMessage();

		Message m1_updated = new Message();
		m1_updated.setText("This is the new text");
		m1_updated.setId(m1.getId());
		m1_updated.setUserId(m1.getUserId());

		given(userRepo.existsById(m1_updated.getUserId())).willReturn(false);
		given(messageRepo.findAll()).willReturn(Arrays.asList(m1));

		ObjectMapper mapper = new ObjectMapper();
		String jsonBodyContent = mapper.writeValueAsString(m1_updated);

		mvc.perform(put("/v1/messages/")
				.content(jsonBodyContent)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void deleteMessage_OK()
			throws Exception {

		Message m1 = getFirstMessage();

		User u1 = new User(0,"mocky");
		u1.setActivetoken("mocktoken");

		given(messageRepo.findById(m1.getId())).willReturn(Optional.of(m1));
		given(userRepo.findByActivetoken(u1.getActivetoken())).willReturn(u1);

		mvc.perform(delete("/v1/messages/id/" + m1.getId())
				.header("Authorization", u1.getActivetoken())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNoContent());

	}

	@Test
	public void deleteMessage_NoMessageWithId_NOTFOUND()
			throws Exception {

		Message m1 = getFirstMessage();

		User u1 = new User(0,"mocky");
		u1.setActivetoken("mocktoken");

		given(messageRepo.findById(m1.getId())).willReturn(java.util.Optional.empty());
		given(userRepo.findByActivetoken(u1.getActivetoken())).willReturn(u1);

		mvc.perform(delete("/v1/messages/id/" + m1.getId())
				.header("Authorization", u1.getActivetoken())
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void deleteMessage_UserMismatch_WithWrongToken_UNAUTHORIZED()
			throws Exception {

		Message m1 = getFirstMessage();

		User u1 = new User(0,"mocky");
		u1.setActivetoken("mocktoken");

		given(messageRepo.findById(m1.getId())).willReturn(Optional.of(m1));
		given(userRepo.findByActivetoken(u1.getActivetoken())).willReturn(u1);

		mvc.perform(delete("/v1/messages/id/" + m1.getId())
				.header("Authorization", "wrongtoken") // Token here is the wrong one, so the server cannot accept the request to delete this message
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnauthorized());
	}
}