package com.hemmingsson.msgboard;

import com.hemmingsson.msgboard.abstractions.UserRepository;
import com.hemmingsson.msgboard.controllers.UserController;
import com.hemmingsson.msgboard.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
@ContextConfiguration(classes=MessageboardApplication.class)
public class UserControllerTests {

    private MockMvc mvc;

    @MockBean
    private UserRepository userRepo;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setUp() {
        //mvc = MockMvcBuilders.standaloneSetup(new HandlerController()).build();
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void login_UserExists_OK()
            throws Exception {

        User mockUser = new User(0,"mocky");

        given(userRepo.getOne(0)).willReturn(mockUser);
        given(userRepo.findByUsernameIgnoreCase("mocky")).willReturn(mockUser);

        mvc.perform(post("/v1/user/?username=mocky&password=st2020")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void loginUser_UserExists_WrongPassword_UNAUTHORIZED()
            throws Exception {

        User mockUser = new User(0,"mocky");

        given(userRepo.getOne(0)).willReturn(mockUser);
        given(userRepo.findByUsernameIgnoreCase("mocky")).willReturn(mockUser);

        mvc.perform(post("/v1/user/?username=mocky&password=someotherpassword")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    /* Bad request is handled by spring automatically
     */
    @Test
    public void loginUser_UserExists_MissingPasswordInRequest_BADREQUEST()
            throws Exception {

        User mockUser = new User(0,"mocky");

        given(userRepo.getOne(0)).willReturn(mockUser);
        given(userRepo.findByUsernameIgnoreCase("mocky")).willReturn(mockUser);

        mvc.perform(post("/v1/user/?username=mocky")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    /* Bad request is handled by spring automatically
     */
    @Test
    public void loginUser_MissingUsernameInRequest_BADREQUEST()
            throws Exception {

        mvc.perform(post("/v1/user/?password=st2020")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void loginUser_UserDoesNotExist_UNAUTHORIZED()
            throws Exception {

        mvc.perform(post("/v1/user/?username=mocky&password=st2020")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void loginUser_UserDoesNotExist_AND_WrongPassword_UNAUTHORIZED()
            throws Exception {

        mvc.perform(post("/v1/user/?username=mocky&password=wrongpassword")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void loginUser_UserDoesExist_MixedCaseInUsername_OK()
            throws Exception {

        User mockUser = new User(0,"mocky");

        given(userRepo.getOne(0)).willReturn(mockUser);
        given(userRepo.findByUsernameIgnoreCase("MoCkY")).willReturn(mockUser);

        mvc.perform(post("/v1/user/?username=MoCkY&password=st2020")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void loginUser_UserDoesExist_MixedCaseInPassword_UNAUTHORIZED()
            throws Exception {

        User mockUser = new User(0,"mocky");

        given(userRepo.getOne(0)).willReturn(mockUser);
        given(userRepo.findByUsernameIgnoreCase("mocky")).willReturn(mockUser);

        mvc.perform(post("/v1/user/?username=mocky&password=ST2020")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }
}
