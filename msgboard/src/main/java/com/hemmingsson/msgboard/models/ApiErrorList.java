package com.hemmingsson.msgboard.models;

import java.util.ArrayList;

public class ApiErrorList {
    private ArrayList<ApiError> errors;

    public ApiErrorList(ArrayList<ApiError> errorList){
        errors = errorList;
    }

    public ArrayList<ApiError> getErrors() {
        return errors;
    }
}
