package com.hemmingsson.msgboard.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class User {
    private @Id @GeneratedValue Integer _id;
    private String username;
    private String activetoken;

    public User() {}

    public User(String userName) {
        this.username = userName;
    }

    public User(int id, String userName) {
        _id = id;
        this.username = userName;
    }
}
