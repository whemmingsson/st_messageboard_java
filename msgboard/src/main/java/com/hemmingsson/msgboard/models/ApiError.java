package com.hemmingsson.msgboard.models;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

@Getter
@Setter
@NoArgsConstructor
/* Based on this RFC: https://tools.ietf.org/html/rfc7807 */
public class ApiError {
    private String type;
    private String title;
    private int status;
    private String detail;
    private String instance;

    public ApiError(String type, String title, String detail, String instance) {
        this.type = type;
        this.title = title;
        this.detail = detail;
        this.instance = instance;
    }

    public ApiError(String type, String title, String detail, String instance, int statusCode) {
        this.type = type;
        this.title = title;
        this.detail = detail;
        this.instance = instance;
        this.status = statusCode;
    }
}
