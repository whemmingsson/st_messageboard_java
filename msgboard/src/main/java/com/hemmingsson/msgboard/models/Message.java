package com.hemmingsson.msgboard.models;

import com.hemmingsson.msgboard.business.ApiSettings;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;

import javax.persistence.*;

@Entity
public class Message {
    private @Id @GeneratedValue Integer _id;

    @Column(length = ApiSettings.MessageMaxLength)
    private String _text;
    private int _userId;
    private boolean _isSanitized;

    private String sanitizeHtml(String html){
        PolicyFactory policy = new HtmlPolicyBuilder()
                .allowAttributes("src").onElements("img")
                .allowAttributes("href").onElements("a")
                .allowStandardUrlProtocols()
                .allowElements("a", "img")
                .toFactory();

        return policy.sanitize(html);
    }

    public Message() {}

    public Message(String text, int userId) {
        setText(text);
        setUserId(userId);
    }

    public int getId(){
        return _id;
    }
    public int getUserId() {
        return _userId;
    }

    public String getText() {
        if(_isSanitized)
            return _text;

        return sanitizeHtml(_text);
    }

    public void setId(int id){
        _id = id;
    }

    public void setUserId(int userId) {
        _userId = userId;
    }

    public void setText(String text) {
        _text = sanitizeHtml(text);
        _isSanitized = true;
    }

    public boolean Equals(Message otherMessage){
        return this.getId() == otherMessage.getId() &&
               this.getUserId() == otherMessage.getUserId();
    }
}
