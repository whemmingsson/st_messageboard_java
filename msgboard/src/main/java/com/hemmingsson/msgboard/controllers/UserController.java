package com.hemmingsson.msgboard.controllers;

import com.hemmingsson.msgboard.business.TokenService;
import com.hemmingsson.msgboard.abstractions.UserRepository;
import com.hemmingsson.msgboard.models.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private final UserRepository _userRepository;

    public UserController(UserRepository userRepo) {
        _userRepository = userRepo;
    }

    @PostMapping("v1/user")
    public ResponseEntity<User> loginUser(@RequestParam("username") String username, @RequestParam("password") String password) {
        User user = _userRepository.findByUsernameIgnoreCase(username);

        // Here, in the full-fledged API app, we would of course validate a real password.
        if(user == null || !password.equals("st2020")){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        TokenService ts = new TokenService();
        String token = ts.getToken(username);

        user.setActivetoken(token);
        _userRepository.save(user);

        return new ResponseEntity(user, HttpStatus.OK);
    }
}
