package com.hemmingsson.msgboard.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.hemmingsson.msgboard.abstractions.*;
import com.hemmingsson.msgboard.business.*;
import com.hemmingsson.msgboard.models.*;

@Slf4j
@RestController
public class MessageController {

    @Autowired
    private MessageSource _messageSource;

    private final String _messageSourceBaseKey = "api-messages-";

    private final MessageRepository _messageRepository;
    private final UserRepository _userRepository;

    private boolean userExists(int id) {
        return _userRepository.existsById(id);
    }

    private boolean userOwnsMessage(String token, int messageUserId){
        User user = _userRepository.findByActivetoken(token);

        if(user == null)
            return false;

        return user.get_id() == messageUserId;
    }

    private String getResponseText(String key, Object[] params){
        if(params == null) {
            return _messageSource.getMessage(_messageSourceBaseKey + key, null, Locale.ENGLISH);
        }
        else {
            return _messageSource.getMessage(_messageSourceBaseKey + key, params, Locale.ENGLISH);
        }
    }

    private String getResponseText(String key){
        return getResponseText(key, null);
    }

    private String getCurrentUriString(){
        return ServletUriComponentsBuilder.fromCurrentRequestUri().host(null).port(null).scheme(null).build().toUriString();
    }

    private ApiError getServerErrorMessage(String uri, Exception exception){
        return new ApiError("/errors/internal-server-error",
                getResponseText("internal-server-error-title"),
                getResponseText("internal-server-error-details", new Object[] {exception.getMessage()}),
                uri,
                HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    public MessageController(MessageRepository msgRepo, UserRepository userRepo) {
        _messageRepository = msgRepo;
        _userRepository = userRepo;
    }

    @GetMapping("v1/messages/all")
    public ResponseEntity getAllMessages() {
        try {
            return new ResponseEntity(_messageRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(getServerErrorMessage(getCurrentUriString(), e));
        }
    }

    @GetMapping("v1/messages/id/{id}")
    public ResponseEntity getMessageById(@PathVariable int id) {
        try {
            Optional<Message> optMsg = _messageRepository.findById(id);

            if (!optMsg.isPresent()) {
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body(new ApiError("/errors/validation/message-id-not-found",
                                getResponseText("get-message-id-not-found-title", new Object[] {id}),
                                getResponseText("get-message-id-not-found-details", new Object[] {id}),
                                getCurrentUriString(),
                                HttpStatus.NOT_FOUND.value()));
            }

            return new ResponseEntity<>(optMsg.get(), HttpStatus.OK);

        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(getServerErrorMessage(getCurrentUriString(), e));
        }
    }

    @GetMapping("v1/messages/userid/{userId}")
    public ResponseEntity getMessagesByUserId(@PathVariable int userId) {
        try {
            List<Message> all = _messageRepository.findAll();

            if (!all.stream().anyMatch(m -> m.getUserId() == userId)) {
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body(
                        new ApiError("/errors/validation/messages-not-found",
                                getResponseText("get-messages-not-found-title", new Object[] {userId}),
                                getResponseText("get-messages-not-found-details", new Object[] {userId}),
                                getCurrentUriString(),
                                HttpStatus.NOT_FOUND.value())
                );
            }

            return new ResponseEntity<>(all.stream()
                    .filter(m -> m.getUserId() == userId)
                    .collect(Collectors.toList()), HttpStatus.OK);

        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(getServerErrorMessage(getCurrentUriString(), e));
        }
    }

    @PostMapping("v1/messages")
    public ResponseEntity postNewMessage(@RequestHeader(ApiSettings.JWTAuthHeader) String token, UriComponentsBuilder builder, @RequestBody Message message) {
        try {

            // URI for the current request - we use it only in possible error and validation messages
            String uri = getCurrentUriString();

            if(!userOwnsMessage(token, message.getUserId())) {
                return ResponseEntity
                        .status(HttpStatus.UNAUTHORIZED)
                        .body(new ApiError("/errors/unauthorized/user-mismatch",
                                getResponseText("post-message-user-mismatch-title"),
                                getResponseText("post-message-user-mismatch-details"),
                                getCurrentUriString(),
                                HttpStatus.UNAUTHORIZED.value()));
            }


            // We use an array here since there can be multiple errors at once in the request.
            // In that case, we should send all messages back to the client, to simplify debugging on their side.
            ArrayList<ApiError> errors = new ArrayList<ApiError>();

            if (!userExists(message.getUserId())) {
                errors.add(new ApiError("/errors/validation/user-missing",
                        getResponseText("post-user-missing-title", new Object[] {message.getUserId()}),
                        getResponseText("post-user-missing-details", new Object[] {message.getUserId()}),
                        uri,
                        HttpStatus.BAD_REQUEST.value()));
            }

            if(message.getText() == null || message.getText().length() == 0){
                errors.add(new ApiError("/errors/validation/message-missing",
                        getResponseText("post-message-missing-title"),
                        getResponseText("post-message-missing-details"),
                        uri,
                        HttpStatus.BAD_REQUEST.value()));
            }

            if(message.getText().length() > ApiSettings.MessageMaxLength){
               errors.add(new ApiError("/errors/validation/message-too-long",
                        getResponseText("post-message-too-long-title"),
                        getResponseText("post-message-too-long-details", new Object[]{ApiSettings.MessageMaxLength}),
                        uri,
                        HttpStatus.BAD_REQUEST.value()));
            }

            if(errors.size() > 1) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiErrorList(errors));
            }
            else if(errors.size() == 1){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors.get(0));
            }

            _messageRepository.save(message);

            // Set location header to the newly created resource
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(builder.path("/v1/messages/id/{id}").buildAndExpand(message.getId()).toUri());

            return new ResponseEntity<Message>(message, headers, HttpStatus.CREATED);

        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(getServerErrorMessage(getCurrentUriString(), e));
        }
    }

    @PutMapping("v1/messages")
    public ResponseEntity updateMessage(@RequestHeader(ApiSettings.JWTAuthHeader) String token, UriComponentsBuilder builder, @RequestBody Message message) {
        try {
            String uri = getCurrentUriString();

            // Validate that the user exists. Since this is a form of bad request,
            // we don't include it in the array of validation errors that should return a not found response
            if (!userExists(message.getUserId())) {
                ApiError error = new ApiError("/errors/validation/user-missing",
                        getResponseText("post-user-missing-title", new Object[] {message.getId()}),
                        getResponseText("post-user-missing-details", new Object[] {message.getId()}),
                        uri,
                        HttpStatus.BAD_REQUEST.value());
                return ResponseEntity.badRequest().body(error);
            }

            if(!userOwnsMessage(token, message.getUserId())) {
                return ResponseEntity
                        .status(HttpStatus.UNAUTHORIZED)
                        .body(new ApiError("/errors/unauthorized/user-mismatch",
                                getResponseText("update-message-user-mismatch-title"),
                                getResponseText("update-message-user-mismatch-details"),
                                getCurrentUriString(),
                                HttpStatus.UNAUTHORIZED.value()));
            }

            List<Message> all = _messageRepository.findAll();

            // We use an array here since there can be multiple errors at once in the request.
            // In that case, we should send all messages back to the client, to simplify debugging on their side.
            ArrayList<ApiError> errors = new ArrayList<ApiError>();

            if (!all.stream().anyMatch(m -> m.getId() == message.getId())) {
                errors.add(new ApiError("/errors/validation/message-id-not-found",
                        getResponseText("put-message-id-not-found-title", new Object[] {message.getId()}),
                        getResponseText("put-message-id-not-found-details", new Object[] {message.getId()}),
                        uri,
                        HttpStatus.NOT_FOUND.value()));
            }

            if (!all.stream().anyMatch(m -> m.getUserId() == message.getUserId())) {
                errors.add(new ApiError("/errors/validation/message-user-id-not-found",
                        getResponseText("put-message-user-id-not-found-title", new Object[] {message.getUserId()}),
                        getResponseText("put-message-user-id-not-found-details", new Object[] {message.getUserId()}),
                        uri,
                        HttpStatus.NOT_FOUND.value()));
            }

            if(errors.size() > 1) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ApiErrorList(errors));
            }
            else if(errors.size() == 1){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errors.get(0));
            }

           Message repoMessage = all.stream().filter(m -> m.Equals(message)).findFirst().get();
            repoMessage.setText(message.getText());
            _messageRepository.save(repoMessage);

            // Set location header to the updated resource
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(builder.path("/v1/messages/id/{id}").buildAndExpand(message.getId()).toUri());

            return new ResponseEntity<>(repoMessage, headers, HttpStatus.OK);

        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(getServerErrorMessage(getCurrentUriString(), e));
        }
    }

    @DeleteMapping("v1/messages/id/{id}")
    public ResponseEntity deleteMessage(@RequestHeader(ApiSettings.JWTAuthHeader) String token, @PathVariable int id) {
        try {

            Optional<Message> optMsg = _messageRepository.findById(id);

            if (!optMsg.isPresent()) {
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body(new ApiError("/errors/validation/message-id-not-found",
                            getResponseText("delete-message-id-not-found-title", new Object[] {id}),
                            getResponseText("delete-message-id-not-found-details", new Object[] {id}),
                            getCurrentUriString(),
                            HttpStatus.NOT_FOUND.value()));
            }

            if(!userOwnsMessage(token, optMsg.get().getUserId())) {
                return ResponseEntity
                        .status(HttpStatus.UNAUTHORIZED)
                        .body(new ApiError("/errors/unauthorized/user-mismatch",
                                getResponseText("delete-message-user-mismatch-title"),
                                getResponseText("delete-message-user-mismatch-details"),
                                getCurrentUriString(),
                                HttpStatus.UNAUTHORIZED.value()));
            }

            _messageRepository.delete(optMsg.get());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(getServerErrorMessage(getCurrentUriString(), e));
        }
    }
}

