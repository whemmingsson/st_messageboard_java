package com.hemmingsson.msgboard.business;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

public class TokenService {
    public String getToken(String username) {
        String token = Jwts
                .builder()
                .setId("msgboardJWT")
                .setSubject(username)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + ApiSettings.JWTLifetime))
                .signWith(SignatureAlgorithm.HS512, ApiSettings.JWTSecret.getBytes()).compact();

        return ApiSettings.JWTAuthHeaderValuePrefix + token;
    }

    public Claims validateToken(String token) {
        return Jwts.parser()
                .setSigningKey(ApiSettings.JWTSecret.getBytes())
                .parseClaimsJws(token)
                .getBody();
    }
}
