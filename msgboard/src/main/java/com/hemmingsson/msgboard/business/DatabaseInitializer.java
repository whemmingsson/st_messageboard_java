package com.hemmingsson.msgboard.business;

import com.hemmingsson.msgboard.abstractions.MessageRepository;
import com.hemmingsson.msgboard.abstractions.UserRepository;
import com.hemmingsson.msgboard.models.Message;
import com.hemmingsson.msgboard.models.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
class DatabaseInitializer {

    @Bean
    CommandLineRunner initUserDatabase(UserRepository repository) {
        return args -> {
            log.info("Seeding user database with " + repository.save(new User(1,"turing")));
            log.info("Seeding user database with "  + repository.save(new User(2,"neumann")));
        };
    }

    @Bean
    CommandLineRunner initMessageDatabase(MessageRepository repository) {
        return args -> {
            log.info("Seeding messageboard database with " + repository.save(new Message("Sometimes it is the people no one can imagine anything of who do the things no one can imagine", 1)));
            log.info("Seeding messageboard database with "  + repository.save(new Message("In mathematics you don’t understand things. You just get used to them.", 2)));
        };
    }
}
