package com.hemmingsson.msgboard.business;

public class ApiSettings {
    public static final String JWTSecret = "$$jwt1Token2Secret$$";
    public static final int JWTLifetime = 3600000; // Lifetime in ms, 1 hour
    public static final String JWTAuthHeader = "Authorization";
    public static final String JWTAuthHeaderValuePrefix = "Bearer ";

    public static final int MessageMaxLength = 512;
}
