package com.hemmingsson.msgboard.business;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class JWTAuthorizationFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException {
        try {
            if (hasToken(request)) {
                Claims claims = validateToken(request);
                setUpSpringAuthentication(claims);
            }
            chain.doFilter(request, response);

        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException e) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            response.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
        }
        catch (Exception e){
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Unexpected server side error occured");
            log.error(e.getMessage());
        }
    }

    private String getToken(HttpServletRequest request){
        return request.getHeader(ApiSettings.JWTAuthHeader).replace(ApiSettings.JWTAuthHeaderValuePrefix, "");
    }

    private Claims validateToken(HttpServletRequest request) {
        TokenService service = new TokenService();
        return service.validateToken(getToken(request));
    }

    private void setUpSpringAuthentication(Claims claims) {
        @SuppressWarnings("unchecked")
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null,null);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    private boolean hasToken(HttpServletRequest request) {
        String authenticationHeader = request.getHeader(ApiSettings.JWTAuthHeader);
        if (authenticationHeader == null || !authenticationHeader.startsWith(ApiSettings.JWTAuthHeaderValuePrefix))
            return false;
        return true;
    }

}