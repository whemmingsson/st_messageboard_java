package com.hemmingsson.msgboard.abstractions;

import com.hemmingsson.msgboard.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;

/* In memory database using H2 */
public interface MessageRepository extends JpaRepository<Message, Integer> {

}
