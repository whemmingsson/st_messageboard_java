# ST_MessageBoard Java Edition - Quick start

## Overview

* IDE Used: IntelliJ Community Edition
* Bootstrapped with Spring Boot
* Rough worklog (to complement git commit messages) that includes valuable resources used, is found here: https://bitbucket.org/whemmingsson/st_messageboard_java/src/master/msgboard/docs/worklog.txt

## Prerequisites

* Java JRE 1.8 or later
* IDE to easily browse code or build and package target jar

## To run the API

Either:

Download the SNAPSHOT Jar from: https://bitbucket.org/whemmingsson/st_messageboard_java/src/master/msgboard/build/

OR

Clone the repo and open in IDE. With maven installed, project can published and packaged by running "mvn package". This also runs test suite. 

Then

1. Run the jar from a terminal, "java -jar messageboard-0.0.1-SNAPSHOT.jar"

2. Browse http://localhost:8080 with your favorite browser.

3. View api docs on: http://localhost:8080/api/v2/api-docs

4. View swagger UI on: http://localhost:8080/api/swagger-ui.html 

5. Test the different endpoints with wither Swagger or Postman. Right now, all endpoints require a valid token, except: http://localhost:8080/api/v1/messages/all
